<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Bootstrap -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
    <script src="bootstrap/js/bootstrap.min.js"></script>
<title>Hinterstoeder</title>
</head>
<body>

<div class="container-fluid">
    
    <div class="page-header" style="text-align:center; background-color:#0093c8;
     font-family:arial;">
        
        <H3 style="color:lavender;">Vítejte u nás v Hinterstoderu</H3>
        
    </div>
    
    <div class="jumbotron">
        
     <h1 style="color:#0093c8; font-family:arial;">Je tady v zimě krásně</h1>
     <h4 style="color:blue;font-family:arial;">A můžete celý den lyžovat, nebo se koulovat.</h4> 
     
     <div class="float-left">
     		<a class="btn btn-primary" role="button" href="http://localhost:8088/HINTERSTODER/admin.php">�vod</a>
     
    		<a class="btn btn-primary" role="button" href="http://localhost:8088/HINTERSTODER/sklad_ui.php">Sklad</a>
    		
    		<a class="btn btn-primary" role="button" href="http://localhost:8088/HINTERSTODER/administrace.php">Administrace</a>
    	
    		<a class="btn btn-primary" role="button" href="http://localhost:8088/HINTERSTODER/kniha_navstev_ui.php">Kniha n�v�t�v</a>
    	
    		<a class="btn btn-primary" role="button" href="https://calendar.google.com/calendar/b/2/r?tab=wc&pli=1">Kalend��</a>
    		     
    </div>  
    
    <div class="float-right">
    		<a class="btn btn-primary" role="button" href="http://localhost:8088/HINTERSTODER/sklad_admin.php">Nová položka</a>
    </div>
    
    
</div>

<div class="row">
    
<?php
  /* Specify the server and connection string attributes. */  
$serverName = "(local)";
/* Get UID and PWD from application-specific files.  */  
$uid = file_get_contents("c:\development\PHP\uid.txt");  
$pwd = file_get_contents("c:\development\PHP\pwd.txt");
$connectionInfo = array( "UID"=>$uid,  
                         "PWD"=>$pwd,  
                         "Database"=>"hinter"); 
 /* Connect using SQL Server Authentication. */  
$conn = sqlsrv_connect( $serverName, $connectionInfo);  
if( $conn === false )  
{  
     echo "Unable to connect.</br>";  
     die( print_r( sqlsrv_errors(), true));  
}  

/* Query SQL Server for the login of the user accessing the  
database. */  
$tsql = "SELECT CONVERT(varchar(32), SUSER_SNAME())";  
$stmt = sqlsrv_query( $conn, $tsql);  
if( $stmt === false )  
{  
     echo "Error in executing query.</br>";  
     die( print_r( sqlsrv_errors(), true));  
}  
/* Retrieve and display the results of the query. */  
$row = sqlsrv_fetch_array($stmt);  
/*echo "User login: ".$row[0]."";
echo "<br>";*/
$zbozi = "select id, nazev_zbozi, mnozstvi from dbo.tbl_zbozi";
$stmt1 = sqlsrv_query( $conn, $zbozi);
if( $stmt1 === false )
{
	echo "Error in executing query.</br>";
	die( print_r( sqlsrv_errors(), true));
} 
$table = '<table class="table" id="MyTable">
		<thead>
		<tr>
		<th scope="col">id</th>
		<th scope="col">Název</th>
		<th scope="col">Množství</th>
		</tr>
		</thead><tbody>';
while($record = sqlsrv_fetch_array($stmt1))
	{	
		$table .= '<tr><td>'.$record['id'].'</td><td>'.$record['nazev_zbozi'].'</td><td>'.$record['mnozstvi'].'</td></tr>';
	}
	echo $table;
/* Free statement and connection resources. */  
sqlsrv_free_stmt( $stmt);  
sqlsrv_close( $conn);                        
?>
  
</div>
</body>
</html>