<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Bootstrap -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
    <script src="bootstrap/js/bootstrap.min.js"></script>
<title>Hinterstoeder</title>
</head>
<body>

<div class="container-fluid">
    
    <div class="page-header" style="text-align:center; background-color:#0093c8;
     font-family:arial;">
        
        <H3 style="color:lavender;">Vítejte u nás v Hinterstoderu</H3>
        
    </div>
    
    <div class="jumbotron">
        
     <h1 style="color:#0093c8; font-family:arial;">Je tady v zimě krásně</h1>
     <h4 style="color:blue;font-family:arial;">A můžete celý den lyžovat, nebo se koulovat.</h4> 
     
     <div class="float-left">
     		<a class="btn btn-primary" role="button" href="#">Úvod</a>
     
    		<a class="btn btn-primary" role="button" href="#">Sklad</a>
    		
    		<a class="btn btn-primary" role="button" href="#">Administrace</a>
    	
    		<a class="btn btn-primary" role="button" href="#">Kniha návštěv</a>
    	
    		<a class="btn btn-primary" role="button" href="#">Kalendář</a>
    		     
    </div>
    
    
</div>

<div class="row">
    
  <div class="col-sm-4" style="background-color:white; text-align:center;">
    <div class="thumbnail">
         <a href="https://www.hinterstoder.at/cms/winterthemen/winterurlaub-hinterstoder">
                <img src="hinter-hory-pohled.jpg" class="img-rounded" alt="Lights" style="width:60%">
                <div class="caption">
                     <p>Hory jsou okouzlující</p>
                </div>
        </a>
    </div>
    
</div>    
  <div class="col-sm-4" style="background-color:white; text-align:center;">
      <div class="thumbnail">
         <a href="https://www.hinterstoder.at/cms/winterthemen/winterurlaub-hinterstoder">
                <img src="hinter-lanovka-lide.jpg" class="img-rounded" alt="Lights" style="width:60%">
                <div class="caption">
                     <p>Jízda lanovkou vzrušující</p>
                </div>
        </a>
    </div>
  
  </div>
  <div class="col-sm-4" style="background-color:white; text-align:center;">
      <div class="thumbnail">
         <a href="https://www.hinterstoder.at/cms/winterthemen/winterurlaub-hinterstoder">
                <img src="hinter-zima-lide.jpg" class="img-rounded" alt="Lights" style="width:60%">
                <div class="caption">
                     <p>Lidé jsou milí a vstřícní</p>
                </div>
        </a>
    </div>
  </div>
  
  <div class="col-sm-4" style="background-color:white; text-align:center;">
      <div class="thumbnail">
         <a href="https://www.hinterstoder.at/cms/winterthemen/winterurlaub-hinterstoder">
                <img src="hinter-dite-sane.jpg" class="img-rounded" alt="Lights" style="width:60%">
                <div class="caption">
                     <p>Na své si přijdou i ti nejmenší</p>
                </div>
        </a>
    </div>
  </div>
  
  <div class="col-sm-4" style="background-color:white; text-align:center;">
      <div class="thumbnail">
         <a href="https://www.hinterstoder.at/cms/winterthemen/winterurlaub-hinterstoder">
                <img src="hinter-bezky-clovek.jpg" class="img-rounded" alt="Lights" style="width:60%">
                <div class="caption">
                     <p>Máme kvalitní běžecké podmínky</p>
                </div>
        </a>
    </div>
  </div>
  
  <div class="col-sm-4" style="background-color:white; text-align:center;">
      <div class="thumbnail">
         <a href="https://www.hinterstoder.at/cms/winterthemen/winterurlaub-hinterstoder">
                <img src="hinter-snowboard-clovek.jpg" class="img-rounded" alt="Lights" style="width:60%">
                <div class="caption">
                     <p>Kdo se nudí, také nebude zklamán</p>
                </div>
        </a>
    </div>
  </div>
  
</div>
</body>
</html>