<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Bootstrap -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
    <script src="bootstrap/js/bootstrap.min.js"></script>
<title>Hinterstoeder</title>
</head>
<body>

<div class="container-fluid">
    
    <div class="page-header" style="text-align:center; background-color:#0093c8;
     font-family:arial;">
        
        <H3 style="color:lavender;">Vítejte u nás v Hinterstoderu</H3>
        
    </div>
    
    <div class="jumbotron">
        
     <h1 style="color:#0093c8; font-family:arial;">Je tady v zimě krásně</h1>
     <h4 style="color:blue;font-family:arial;">A můžete celý den lyžovat, nebo se koulovat.</h4> 
     
     <div>
	  		<label for="usr">Jméno:</label>
	  		<input type="text" class="form-control" id="usr" name="login">		
	  		<label for="pwd">Heslo:</label>
	  		<input type="password" class="form-control" id="pwd" name="heslo"> <br>
			<input class="btn btn-primary" type="submit" value="Login">
     </div>
     
    </div>    
</div>
<div class="row">
    
  <div class="col-sm-4" style="background-color:white; text-align:center;">
    <div class="thumbnail">
         <a href="https://www.hinterstoder.at/cms/winterthemen/winterurlaub-hinterstoder">
                <img src="hinter-hory-pohled.jpg" class="img-rounded" alt="Lights" style="width:60%">
                <div class="caption">
                     <p>Hory jsou okouzlující</p>
                </div>
        </a>
    </div>
    
</div>    
  <div class="col-sm-4" style="background-color:white; text-align:center;">
      <div class="thumbnail">
         <a href="https://www.hinterstoder.at/cms/winterthemen/winterurlaub-hinterstoder">
                <img src="hinter-lanovka-lide.jpg" class="img-rounded" alt="Lights" style="width:60%">
                <div class="caption">
                     <p>Jízda lanovkou vzrušující</p>
                </div>
        </a>
    </div>
  
  </div>
  <div class="col-sm-4" style="background-color:white; text-align:center;">
      <div class="thumbnail">
         <a href="https://www.hinterstoder.at/cms/winterthemen/winterurlaub-hinterstoder">
                <img src="hinter-zima-lide.jpg" class="img-rounded" alt="Lights" style="width:60%">
                <div class="caption">
                     <p>Lidé jsou milí a vstřícní</p>
                </div>
        </a>
    </div>
  </div>
  
  <div class="col-sm-4" style="background-color:white; text-align:center;">
      <div class="thumbnail">
         <a href="https://www.hinterstoder.at/cms/winterthemen/winterurlaub-hinterstoder">
                <img src="hinter-dite-sane.jpg" class="img-rounded" alt="Lights" style="width:60%">
                <div class="caption">
                     <p>Na své si přijdou i ti nejmenší</p>
                </div>
        </a>
    </div>
  </div>
  
  <div class="col-sm-4" style="background-color:white; text-align:center;">
      <div class="thumbnail">
         <a href="https://www.hinterstoder.at/cms/winterthemen/winterurlaub-hinterstoder">
                <img src="hinter-bezky-clovek.jpg" class="img-rounded" alt="Lights" style="width:60%">
                <div class="caption">
                     <p>Máme kvalitní běžecké podmínky</p>
                </div>
        </a>
    </div>
  </div>
  
  <div class="col-sm-4" style="background-color:white; text-align:center;">
      <div class="thumbnail">
         <a href="https://www.hinterstoder.at/cms/winterthemen/winterurlaub-hinterstoder">
                <img src="hinter-snowboard-clovek.jpg" class="img-rounded" alt="Lights" style="width:60%">
                <div class="caption">
                     <p>Kdo se nudí, také nebude zklamán</p>
                </div>
        </a>
    </div>
  </div>
  
</div>
<?php
  /* Specify the server and connection string attributes. */  
$serverName = "(local)";
/* Get UID and PWD from application-specific files.  */  
$uid = file_get_contents("c:\development\PHP\uid.txt");  
$pwd = file_get_contents("c:\development\PHP\pwd.txt");
$connectionInfo = array( "UID"=>$uid,  
                         "PWD"=>$pwd,  
                         "Database"=>"hinter"); 
 /* Connect using SQL Server Authentication. */  
$conn = sqlsrv_connect( $serverName, $connectionInfo);  
if( $conn === false )  
{  
     echo "Unable to connect.</br>";  
     die( print_r( sqlsrv_errors(), true));  
}  

/* Query SQL Server for the login of the user accessing the  
database. */   
$tsql = "SELECT CONVERT(varchar(32), SUSER_SNAME())";
$stmt = sqlsrv_query( $conn, $tsql);
if( $stmt === false )
{
	echo "Error in executing query.</br>";
	die( print_r( sqlsrv_errors(), true));
}
$row = sqlsrv_fetch_array($stmt);  
echo "User login: ".$row[0]."";
echo "<br>";
$login = $_POST["login"];
$heslo = $_POST["heslo"];
$querry = "select username, password from dbo.tbl_administration where username = '$login'";
$stmt1 = sqlsrv_query( $conn, $querry);
$record = sqlsrv_fetch_array($stmt1);
$message = "";
$correct = $record[1];
if ($heslo == $correct)
{
	header("Location:http://localhost:8088/HINTERSTODER/admin.php");
}
else
{
	$message ="chyba";
}
echo $message;
?>
</body>
</html>