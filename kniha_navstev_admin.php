<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Bootstrap -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
    <script src="bootstrap/js/bootstrap.min.js"></script>
<title>Hinterstoeder</title>
</head>
<body>

<div class="container-fluid">
    
    <div class="page-header" style="text-align:center; background-color:#0093c8;
     font-family:arial;">
        
        <H3 style="color:lavender;">Vítejte u nás v Hinterstoderu</H3>
        
    </div>
    
    <div class="jumbotron">
        
     <h1 style="color:#0093c8; font-family:arial;">Je tady v zimě krásně</h1>
     <h4 style="color:blue;font-family:arial;">A můžete celý den lyžovat, nebo se koulovat.</h4> 
     
     <div class="float-left">
     		<a class="btn btn-primary" role="button" href="#">Úvod</a>
     
    		<a class="btn btn-primary" role="button" href="#">Sklad</a>
    		
    		<a class="btn btn-primary" role="button" href="#">Administrace</a>
    	
    		<a class="btn btn-primary" role="button" href="#">Kniha návštěv</a>
    	
    		<a class="btn btn-primary" role="button" href="#">Kalendář</a>
    		     
    </div>
    
    <div class="float-right">
    		<a class="btn btn-primary" role="button" href="file:///C:/RP-hinter/HINTERSTODER/kniha_navstev_ui.html#">Zpět</a>
    </div>
    
    
</div>

<div class="list">
  <label for="jmeno">Jméno:</label>
  <input type="text" class="form-control" id="jmeno">
  <label for="prijmeni">Příjmení:</label>
  <input type="text" class="form-control" id="prijmeni">
  <label for="checkin">Datum příjezdu:</label>
  <input type="text" class="form-control" id="checkin">	
  <label for="checkout">Datum odjezdu:</label>
  <input type="text" class="form-control" id="checkout">
  <label for="deti_domaci">Děti domácí</label>
  <input type="text" class="form-control" id="deti_domaci">
  <label for="deti_skorodomaci">Děti skoro-domácí</label>
  <input type="text" class="form-control" id="deti_skorodomaci">
  <label for="deti">Děti</label>
  <input type="text" class="form-control" id="deti">
  <label for="domaci">Domácí</label>
  <input type="text" class="form-control" id="domaci">
  <label for="skorodomaci">Skoro-domácí</label>
  <input type="text" class="form-control" id="skorodomaci">
  <label for="dospely">Dospělý</label>
  <input type="text" class="form-control" id="dospely"><br>
  <div class="float-right">
  	<button class="btn btn-primary" type="submit">Uložit</button>
  </div>
</div>
</body>
</html>